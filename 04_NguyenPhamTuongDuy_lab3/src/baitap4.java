
import java.awt.Button;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author root
 */
public class baitap4 extends JFrame{
  
 public baitap4 () {  
     JPanel pnBox = new JPanel();
     pnBox.setLayout(new BoxLayout(pnBox,BoxLayout.Y_AXIS));
     JButton btn1 = new JButton("BoxLayout");
     btn1.setForeground(Color.blue);
     Font font= new Font("Arial",Font.BOLD | Font.ITALIC,25);
     btn1.setFont(font);
     pnBox.add(btn1);
     
     JButton btn2 = new JButton("X_AXIS");
     
     btn2.setForeground(Color.red);
     btn2.setFont(font);
     pnBox.add(btn2);
     
     JButton btn3 = new JButton("X_AXIS");
     btn3.setForeground(Color.yellow);
     btn3.setFont(font);
     pnBox.add(btn3);
     this.add(pnBox);
     
    }  
  
        public static void main(String args[]){  
        baitap4 b=new baitap4();  
        b.setVisible(true);
        b.setBounds(100,200,500,300);
        
        }
    
}
